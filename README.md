# Code server - rocket chat screen spliter

This is a mess of a project to split a web page between a code server and a rocket chat behind a login screen.

### I've highly recomend this to be run behind a reverse web proxy (see https://nginxproxymanager.com) with a wildcard redirect to the nginx server and more redirects to the flask interface and rocket chat server.

Most important settings are in the `docker-compose.yaml` file, some URLs might need to be altered in the html templates.