import os
import secrets
from datetime import timedelta

TITLE = "NES - Programação Orientada a Objetos"

SESSION_TYPE = "filesystem"
SESSION_PERMANENT = True
SESSION_USE_SIGNER = False
PERMANENT_SESSION_LIFETIME = timedelta(days=3)

EDITOR_TOKEN_LIFETIME = timedelta(days=10)

SECRET_KEY = os.getenv("SECRET_KEY", secrets.token_urlsafe(16))

debug = os.getenv("DEBUG")
debug = debug or "false"
DEBUG = debug.lower() == "true"

ROCKET_ADMIN = os.getenv("ROCKET_ADMIN")
ROCKET_PASSWD = os.getenv("ROCKET_PASSWD")
ROCKET_PROF_USER = os.getenv("ROCKET_PROF_USER")
DATA_PATH_HOST = os.getenv('DATA_PATH_HOST')
DATA_PATH = '/data/'
BASE_URL = os.getenv("BASE_URL")
ROCKET_URL = f"https://rocket.{BASE_URL}"

CONFIG_DIR = os.path.join(DATA_PATH, 'config')

ROCKET_PROXY = """server {
    listen 80;
    listen [::]:80;

    server_name rocket."""+BASE_URL+""" www.rocket."""+BASE_URL+""";
        
    location / {
        proxy_pass http://rocket:3000/;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}"""

PERMISSIONS = {
    "admin": [
        "static",
        "login",
        "users",
        "split",
        "/",
        "/.",
        "edit_password",
        "logout",
    ],
    "user": [
        "static",
        "login",
        "split",
        "/",
        "/.",
        "edit_password",
        "logout",
    ],
    "guest": ["static", "login"],
}
"""Mapping of permissions to routes that can be accessed by roles"""

