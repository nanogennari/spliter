from . import app
from .database import (
    get_users,
    get_user,
    init_db,
    insert_user,
)
import click
from getpass import getpass
from .settings import PERMISSIONS, DATA_PATH, ROCKET_PROXY
import os
import pandas as pd
import secrets
from unidecode import unidecode

@app.cli.command("init", help="Inializes database and create Administrator user.")
def init_db_cli():
    init_db()
    with open(os.path.join(DATA_PATH, 'nginx', 'rocket-proxy.conf'), 'w') as f:
        f.write(ROCKET_PROXY)   


@app.cli.group("users", help="Users related commands.")
def users():
    pass


@click.command("list", help="Print list of active users.")
def users_ls():
    for user in get_users():
        print(user.id, user.username, user.name)


@click.command("add", help="Add a single user.")
def user_add():
    username = input("Username: ")
    name = input("Full Name: ")
    passwd = ""
    passwd_conf = "a"
    while passwd != passwd_conf:
        passwd = getpass("Password: ")
        passwd_conf = getpass("Confirm password: ")
    role = ""
    while role not in PERMISSIONS.keys():
        role = input("User role: ")
        if role not in PERMISSIONS.keys():
            print()
            print("Available roles:")
            for r in PERMISSIONS.keys():
                print(f"    - {r}")
            print()
    if get_user(username, by="username") is None:
        user = insert_user(
            name=name,
            username=username,
            password=passwd,
            role=role,
            creator=1,
        )
        print(f"User {username} has successfully registered!")
    else:
        print(f"User {username} already exists!")
    print(get_user(1).name)
    

@click.command("batch-add", help="Add a group of users from a CSV file.")
@click.argument("csv_file")
def users_batch_add(csv_file):
    df = pd.read_csv(csv_file)
    for _, row in df.iterrows():
        password = secrets.token_hex()[:10]
        username = unidecode(row['Nome'].lower().strip().split(' ')[0] + '.' + row['Sobrenome'].strip().split(' ')[-1].lower())
        print("""---------------------------------------
"""+row['Email'].lower()+"""
---------------------------------------
Olá """+row['Nome']+""",

Segue seus dados de acesso a plataforma que utilizaremos para as aulas de Programação orientada a objetos:

Usuário: """+username+"""
Senha: """+password+"""

A plataforma é acessada no endereço: https://nes-poo.nngn.net/

Até a aula,
Juliano Genari""")
        insert_user(
            name = row['Nome'] + " " + row['Sobrenome'],
            username=username,
            password=password,
            role='user',
            creator=1
        )
        #sleep(20)
    

users.add_command(users_ls)
users.add_command(user_add)
users.add_command(users_batch_add)
