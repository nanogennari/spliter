$(document).ready(function () {
    M.AutoInit();
});

function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
}

function generateRandomPassword() {
    let randomPassword = randomString(12);
    $("#new_password").val(randomPassword);
    $("#new_password_text").val(randomPassword);
    $("#password_check").val(randomPassword);
    $("#check").hide();
    $("#random").hide();
    $("#new").hide();
    $("#text").show();
    M.updateTextFields();
    $("#new_password_text").on('change', function (e) {
        $("#new_password_text").val(randomPassword);
    });
}
