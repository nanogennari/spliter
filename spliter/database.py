from . import db, chat, docker
from werkzeug.security import generate_password_hash
import random, string
from datetime import datetime
from .settings import DATA_PATH, CONFIG_DIR, DATA_PATH_HOST, EDITOR_TOKEN_LIFETIME, BASE_URL
import os
import time
import secrets
from datetime import datetime

## DB tables
class CreationControl:
    created: db.Mapped[datetime] = db.mapped_column(nullable=False)
    created_by: db.Mapped[int] = db.mapped_column(db.ForeignKey("users.id"))
    is_deleted: db.Mapped[bool] = db.mapped_column(nullable=False)
    deleted: db.Mapped[datetime] = db.mapped_column(nullable=True)
    deleted_by: db.Mapped[int] = db.mapped_column(db.Integer, nullable=True)

    # Relations
    @db.declared_attr
    def creator(cls):
        return db.relationship("User", uselist=False)


class User(CreationControl, db.Model):
    __tablename__ = "users"

    # Columns
    id: db.Mapped[int] = db.mapped_column(primary_key=True)
    name: db.Mapped[str] = db.mapped_column(nullable=False)
    username: db.Mapped[str] = db.mapped_column(unique=True, nullable=False)
    password: db.Mapped[str] = db.mapped_column(nullable=False)
    role: db.Mapped[str] = db.mapped_column(nullable=False)
    editor_token: db.Mapped[str] = db.mapped_column(nullable=True)
    token_date: db.Mapped[datetime] = db.mapped_column(nullable=True)


## Manipulation functions
def del_controled(obj, user_id):
    if not obj.is_deleted:
        obj.is_deleted = True
        obj.deleted = datetime.now()
        obj.deleted_by = user_id
        db.session.commit()


def get_user(idt: int, by: str = "id") -> User:
    return get_by(User, by, idt, True)


def get_editor_token(user_id):
    user = get_user(user_id)
    
    if user.editor_token and (datetime.now() < (user.token_date + EDITOR_TOKEN_LIFETIME)):
        return user.editor_token
    else:
        code_token = secrets.token_hex()[:16]
        
        code_domain = f"{code_token}.{BASE_URL}"
        
        proxy_file = os.path.join(DATA_PATH, 'nginx', f'{user.username}.conf')
        proxy_config = """server {
    listen 80;

    server_name """+code_domain+""";
        
    location / {
        proxy_pass http://host.docker.internal:"""+str(30000 + user.id)+"""/;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection upgrade;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}"""

        print(f'Reloading proxy for {user.username}.')
        with open(proxy_file, 'w') as f:
            f.write(proxy_config)
        print(docker.containers.get("spliter-proxy-1").exec_run("service nginx reload", user='root'))
        
        user.editor_token = code_token
        user.token_date = datetime.now()
        db.session.commit()
        
        return code_token
        
    

def get_by(table: db.Model, col: str, idt: int, filter_deleted=False):
    """
    Retrieves a user from the database by either their ID or username.

    :param identification: An integer representing the user's ID or a string representing their username.
    :param by: A string indicating whether to search by ID or username. Defaults to "id".
    :return: A tuple representing the user's information from the database or None if not found.
    :raises: Exception if an invalid 'by' parameter is provided.
    """
    if col == "id":
        return db.session.get(table, idt)
    else:
        filter_pars = {col: idt}
        if filter_deleted:
            filter_pars["is_deleted"] = False
        try:
            return db.session.execute(
                db.select(table).filter_by(**filter_pars)
            ).scalar_one()
        except db.orm.exc.NoResultFound:
            return None


def insert_user(username: str, password: str, role: str, creator: int, **kwargs):
    """
    Inserts a user into the database.

    :param username: A string representing the user's username.
    :param hashed_password: A string representing the user's hashed password.
    :param role: A string representing the user's role. Can be "user" or "admin".
    :param kwargs: Additional parameters where "name" is the user's name.
    """
    name = kwargs.get("name", "")
    user = User(
        name=name,
        username=username,
        password=generate_password_hash(password),
        role=role,
        created=datetime.now(),
        created_by=creator,
        is_deleted=False,
    )
    db.session.add(user)
    db.session.commit()
    chat.users_delete(username)
    chat.users_create(
        f"{username}@nes.poo",
        name,
        password,
        username,
        active=True,
        roles=['user'],
        joinDefaultChannels=True,
    ).json()
    user_path = os.path.join(DATA_PATH_HOST, user.username)
    #Popen(['cp', '-r', CONFIG_DIR, user_path], stdin=None, stdout=None, stderr=None)
    volumes = {user_path: {'bind': '/config', 'mode': 'rw'}}
    if role == 'admin':
        volumes.update({DATA_PATH_HOST: {'bind': '/config/workspace/all', 'mode': 'rw'}})
    docker.containers.run(
        "lscr.io/linuxserver/code-server:latest",
        hostname=f"code-{user.id}",
        mem_limit='512m',
        name=f"code-{user.username}",
        ports={'8443/tcp': 30000+user.id},
        volumes=volumes,
        environment={
            'PUID':1000,
            'PGID':1000,
            'TZ': 'America/Maceio',
            'VSCODE_PROXY_URI': '/code',
        },
        detach=True,
    )
    return user


def update_user_password(identification: int, new_password: str, by: str = "id"):
    """
    Updates a user's password.
    """
    user = get_user(identification, by)
    user.password = generate_password_hash(new_password)
    db.session.commit()


def get_users():
    """
    Retrieves all users from the database.

    :return: A list of dictionaries, where each dict represents a row in the users table.
    """
    users = db.session.execute(
        db.select(User).filter_by(is_deleted=False).order_by(User.username)
    ).scalars()
    return users


def init_db():
    """Initializes the database."""
    print("Initializing database...")
    db.create_all()
    print(get_user("admin", "username"))
    if not get_user("admin", "username"):
        print("Creatting administrator user.")
        password = "".join(random.choices(string.ascii_uppercase + string.digits, k=12))
        print("----------------------------------")
        print("Username: admin")
        print("Password:", password)
        print("----------------------------------")
        user = insert_user(
            name="Adminstrator",
            username="admin",
            password=password,
            role="admin",
            creator=1,
        )
        user.created_by = user.id
        db.session.commit()
    print("Database initialized.")
