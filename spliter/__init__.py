from flask import Flask
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from .settings import *
from rocketchat_API.rocketchat import RocketChat
import docker
import time
from flask_migrate import Migrate


db = SQLAlchemy()
app = Flask(__name__)
docker = docker.from_env()
migrate = Migrate(app, db)

with open(os.path.join(DATA_PATH, 'nginx', 'rocket-proxy.conf'), 'w') as f:
    f.write(ROCKET_PROXY)

docker.containers.get("spliter-proxy-1").reload()

time.sleep(10)

chat = RocketChat(ROCKET_ADMIN, ROCKET_PASSWD, server_url=ROCKET_URL)

app.config["DEBUG"] = DEBUG
# To not receive RuntimeError talking that ths session is unavailable beaceuse no secret key was set.
app.config["SESSION_TYPE"] = SESSION_TYPE
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///spliter.sqlite"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.secret_key = SECRET_KEY
Session(app)
db.init_app(app)

from . import index
from . import auth
from . import cli

app.jinja_env.globals.update(
    has_permition=auth.role_has_permission,
    title=TITLE,
    base_url=BASE_URL,
)
