from . import app, chat
from flask import render_template, session
from .settings import ROCKET_URL, DATA_PATH, BASE_URL
from .database import get_editor_token


@app.route('/split')
@app.route('/')
def split():
    token = chat.users_create_token(username=session["user"]['username']).json()
    if 'data' in token:
        token = token['data']
    else:
        print(token)

    chat_url = f"{ROCKET_URL}/channel/Aula"
    
    code_token = get_editor_token(session['user']['id'])
    
    code_domain = f"{code_token}.{BASE_URL}"
    code_url = f"https://{code_domain}"
        
    return render_template('split.html', token=token, chat_url=chat_url, code_url=code_url, rocket_url=ROCKET_URL)