FROM python:3.10-slim

EXPOSE 42000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

WORKDIR /app
COPY . /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD} 

# Install pip requirements
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

USER root

RUN export SECRET_KEY=`python -c 'import secrets; print(secrets.token_hex())'`

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--reload", "--bind", "0.0.0.0:80", "spliter:app"]
